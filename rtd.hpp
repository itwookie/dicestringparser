#ifndef __RTD_HPP__
#define __RTD_HPP__

#include <memory>
#include <functional>

namespace RTD {
    extern std::function<int(int)> RNG;

    // Generischer super-typ.
    // subklassen f�r verwendung nicht wichtig, desshalb in cpp
    struct node {
        virtual double get() = 0;
    };

    std::unique_ptr<node> parseDieString(const char* formula);

    class Dice {
    private:
        std::shared_ptr<node> formula;
    public:
        inline Dice(const char* formula) : formula(std::move(parseDieString(formula))) {

        }
        inline int roll() {
            return (int)(formula->get());
        }
        static int roll(const char* formula) {
            return Dice(formula).roll();
        }
    };

}
#endif // __RTD_HPP__
