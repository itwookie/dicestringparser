# Dice String Parser

Syntax supports addition, subtraction, multiplication, division and grouping.

The operator for rolling dice is a lower-case d, and requires numbers to the left
and right. This will then roll LEFT amount of dice with RIGHT eyes.

Use the same syntax for rolling dice with an upper-case D to get the average result
instead of a rolled one.

Throws `std::runtime_error` when parsing fails.

Don't forget to call srand() or to replace RTD::RNG with your own int(int) funktor.

# Examples

**Roll predefined dice over and over again**
```c++
int DEX_MOD = 3, AC = 12;
char buffer[512] = {0};
sprintf(buffer, "1d20 + %d", DEX_MOD);
RTD::Dice attackRoll(buffer);
sprintf(buffer, "1d4 + %d", DEX_MOD);
RTD::Dice damageRoll(buffer);

int result = attackRoll.roll();
bool hit = result>AC || result == 20;
std::cout << "You " << (hit ? "hit" : "miss") << " (" << result << " vs " << AC << ")" << std::endl;
if (hit)
	std::cout << "You deal " << damageRoll.roll() << " points of damage" << std::endl;
```

**Interactive parser**
```c++
for (std::string line; std::getline(std::cin, line);) {
	if (line.empty()) return 0;
	try {
		std::cout << RTD::Dice::roll(line.c_str()) << std::endl;
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}
```