#include "rtd.hpp"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdlib.h>

int main(int args, char** argv) {
	srand(time(NULL));

	if (args < 2) {
        std::cerr << "Syntax: " << (args==0?"rtd":argv[0]) << "<FORMEL>" << std::endl;
        return 1;
	}
	std::stringstream whole;
	for (int i = 1; i < args; i++) {
        whole << argv[i];
	}
    try {
        std::cout << RTD::Dice::roll(whole.str().c_str()) << std::endl;
        return 0;
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
