#include "rtd.hpp"
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include <stdexcept>
#include <functional>
#include <cstring>

namespace RTD {
    // Funktor, der sp�ter irgendo �berschrieben werden kann
    std::function<int(int)> RNG = [](int eyes){return (rand()%eyes)+1;};

    // Operatoren hilfsfunktionen, die in mathNodes als Funktoren verwendet werden
    double ADD(double a,double b){return a+b;};
    double SUB(double a,double b){return a-b;};
    double MULT(double a,double b){return a*b;};
    double DIV(double a,double b){return a/b;};
    double ROLL(double a,double b){
        int accu = 0;
        for (int i = 0; i < a; i++) { //a z�hlt die anzahlt der w�rfel
            accu += RNG((int)b);
        }
        return (double)accu;
    };
    double ROLL_AVG(double a,double b){ //durchschnits-wert f�r a w�rfel
        //         vvvvvvvvv--- durchschnits-wert eines w�rfels
        return a * ((b+1)/2);
    };

    // Hilfstrukturen zum parsen

    // eine Zahlen-literal
    struct valueNode : node {
        double value;
        valueNode(int value) : value((double)value) {}
        double get() { return value; }
    };
    // ein meta typ, der aus dem string entsteht und von dem alle in mathNodes konvertiert werden m�ssen
    struct tokenNode : node { //template konstrukt, um tokens reduzieren zu k�nnen
        std::string token;
        tokenNode(std::string&& value) : token(value) {}
        double get() { return 0.0; }
    };
    // eine mathematische operation, abh�ngig vom operator
    struct mathNode : node {
        std::function<double(double,double)> op;
        char charop; // f�t eventuelle ausgabe auf std::out
        std::unique_ptr<node> left;
        std::unique_ptr<node> right;
        mathNode (char op, std::unique_ptr<node>& left, std::unique_ptr<node>& right) : charop(op), left(std::move(left)), right(std::move(right)) {
            switch(op) {
                case 'd': { this->op = ROLL; break; }
                case 'D': { this->op = ROLL_AVG; break; }
                case '*': { this->op = MULT; break; }
                case '/': { this->op = DIV; break; }
                case '+': { this->op = ADD; break; }
                case '-': { this->op = SUB; break; }
                default: throw std::runtime_error("Unknown operator");
            }
        }
        double get() {
            return op(left->get(), right->get());
        }
    };

    // Hilfsfunktionen zum parsen

    //typcheck der node auf tokenNode und pr�fung des wertes in tokenNode auf token, liefert iterator auf erstes ergebnis oder end()
    std::vector<std::unique_ptr<node>>::iterator find_token(std::vector<std::unique_ptr<node>>::iterator from, std::vector<std::unique_ptr<node>>::iterator to, const char* token) {
        return std::find_if(from, to, [token](std::unique_ptr<node>& elem){
            tokenNode* asToken = dynamic_cast<tokenNode*>(elem.get());
            return asToken != nullptr && token == asToken->token;
        });
    }
    //typcheck der node auf tokenNode und pr�fung des wertes in tokenNode auf token, liefert reverse_iterator auf letztes ergebnis oder rend()
    std::vector<std::unique_ptr<node>>::reverse_iterator find_token(std::vector<std::unique_ptr<node>>::reverse_iterator from, std::vector<std::unique_ptr<node>>::reverse_iterator to, const char* token) {
        return std::find_if(from, to, [token](std::unique_ptr<node>& elem){
            tokenNode* asToken = dynamic_cast<tokenNode*>(elem.get());
            return asToken != nullptr && token == asToken->token;
        });
    }
    //pr�ft den typ der node in einem unique_ptr iterator
    template<typename Type>
    bool is_nodetype(std::vector<std::unique_ptr<node>>::iterator it) {
        static_assert(std::is_base_of<node, Type>::value, "Type is no subclass of node");
        return dynamic_cast<Type*>(it->get()) != nullptr;
    }
    //pr�ft den typ der node in einem unique_ptr
    template<typename Type>
    bool is_nodetype(std::unique_ptr<node>& value) {
        static_assert(std::is_base_of<node, Type>::value, "Type is no subclass of node");
        return dynamic_cast<Type*>(value.get()) != nullptr;
    }

    //konsumiert formel gruppen. eine gruppe ist zwischen klammern, um die priorit�t zu �ndern. z.B. 1*(2+3)
    // die ganze formel ist auch eine gruppe.
    std::unique_ptr<node> parseDieTokenGroup(std::vector<std::unique_ptr<node>>::iterator a_from, std::vector<std::unique_ptr<node>>::iterator a_to) {
        // sub-vektor hier sammeln
        std::vector<std::unique_ptr<node>> tree;
        if (a_from+1 == a_to)
            throw std::runtime_error("Parse-Fehler: (Unter-)Gruppe leer");
        //notiz zu move: ver�ndert nicht die l�nge! d.h. die pointer sind nach dem move leer
        std::move(a_from, a_to, std::back_inserter(tree));

        // gruppen, klammern
        while (true) {
            auto from = find_token(tree.begin(), tree.end(), "(");
            // vom ende aus suchen, aber vorw�rts it zru�ckgeben. sollte 1 nach dem element stehen.
            auto to = find_token(tree.rbegin(), tree.rend(), ")").base();
            if (from == tree.end() && to == tree.begin()) break; //no more brackets
            if (from == tree.end() || to < from+1) // wenn klammern r�ckw�rts sind sage ich einfach mal es fehl eine (
                throw std::runtime_error("Token erwartet: ')' ohne passendes '('");
            if (to == tree.begin())
                throw std::runtime_error("Token erwartet: '(' ohne passendes ')'");
            if (to == from+1)
                throw std::runtime_error("Token erwartet: in leeren Klammern '()'");
            // untergruppe rekursiv parsen
            std::unique_ptr<node> replacement = parseDieTokenGroup(from+1, to-1); //klammern nicht mit �bergeben
            //und in diesen vektor einsetzen
            tree.erase(from, tree.insert(to, std::move(replacement)));
        }

        /* un�res minus */ {
            size_t offset = 0;
            while (true) {
                auto it = find_token(tree.begin()+offset, tree.end(), "-");
                if (it == tree.end()) break;
                //nicht nochmal von forne suchen, sonst finden wir immer wieder bin�re minusis
                offset = std::distance(tree.begin(), it);

                // muss nach gruppen entweder nach einem token oder am anfang stehen
                if (it == tree.begin() || is_nodetype<tokenNode>(*(it-1))) {
                    // ist der nachfolger keine zahl ist das vorzeichen ung�ltig
                    if ((it+1) != tree.end() && is_nodetype<valueNode>(*(it+1))) {
                        valueNode* current = dynamic_cast<valueNode*>((it+1)->get());
                        int newValue = -((int)(current->get()));
                        auto insert = tree.insert(it+2, std::make_unique<valueNode>(newValue)); //vergr��ern des vektors kann reallokieren -> iteratoren werden ung�ltig
                        tree.erase(tree.begin()+offset, insert);
                    } else {
                        throw std::runtime_error("Nummer erwartet: Nach un�rem '-'");
                    }
                } // sonst: kein un�res minus
            }
        }

        const char * tokens[3][2] = {
            { "d", "D" },
            { "*", "/" },
            { "+", "-" }
        };
        for (int row = 0; row < 3; row++) { // h�her als punkt und strich operatoren
            size_t offset = 0;
            while (true) {
                // immer 2 tokens haben gleiche priorit�t, fr�hestes suchen
                auto it = find_token(tree.begin()+offset, tree.end(), tokens[row][0]);
                auto it2 = find_token(tree.begin()+offset, tree.end(), tokens[row][1]);
                if (it2 < it) it = it2;
                if (it == tree.end()) break; //doch nix -> fertig
                offset = std::distance(tree.begin(), it);

                if (row == 0) {
                    //ausdruck ist fix: NUMMER 'd' NUMMER -> links und rechts M�SSEN valueNodes sein
                    if (it == tree.begin() || !is_nodetype<valueNode>(it-1))
                        throw std::runtime_error("Nummer erwartet: In DICE-Ausdruck vor 'd'");
                    if (it >= tree.end()-1 || !is_nodetype<valueNode>(it+1))
                        throw std::runtime_error("Nummer erwartet: in DICE-Ausdruck nach 'd'");
                    if ((dynamic_cast<valueNode*>((it-1)->get())->get())<=0)
                        throw std::runtime_error("Positive Anzahl erwartet: in DICE-Ausdruck vor 'd', war <= 0");
                    if ((dynamic_cast<valueNode*>((it+1)->get())->get())<=0)
                        throw std::runtime_error("Positive Augenzahl erwartet: in DICE-Ausdruck nach 'd', war <= 0");
                } else {
                    if (it == tree.begin() || is_nodetype<tokenNode>(it-1)) {
                        std::stringstream msg;
                        msg << "Nummer oder Gruppe erwartet: in Mathe-Ausdruck vor '" << (dynamic_cast<tokenNode*>(it->get())->token) << "'";
                        throw std::runtime_error(msg.str().c_str());
                    }
                    if (it >= tree.end()-1 || is_nodetype<tokenNode>(it+1)) {
                        std::stringstream msg;
                        msg << "Nummer oder Gruppe erwartet: in Mathe-Ausdruck nach '" << (dynamic_cast<tokenNode*>(it->get())->token) << "'";
                        throw std::runtime_error(msg.str().c_str());
                    }
                }

                tokenNode* tn_ptr = dynamic_cast<tokenNode*>(it->get());
                char token = tn_ptr->token[0];
                tree.erase(it-1, tree.insert(it+2, std::make_unique<mathNode>(token, *(it-1), *(it+1))));

            }
        }

        //jetzt sollte tree l�nge 1 haben
        if (tree.size() != 1)
            throw std::runtime_error("Parse-Fehler: Nicht vollst�ndig aufgel�st!");

        return std::move(*tree.begin());
    }
    //zerlegt einen tring in die tokens, ignoriert leerzeichen und gibt die formel zur�ck
    std::unique_ptr<node> parseDieString(const char* formula) {
        int at = 0;
        int end = strlen(formula);
        std::vector<std::unique_ptr<node>> tokens;
        // in tokens zerlegen
        for (;at < end;) {
            //leerzeichen ignorieren
            while (formula[at]==' ') { if (at >= end) break; at++; }
            if (formula[at] == '(' ||
                formula[at] == ')' ||
                formula[at] == '+' ||
                formula[at] == '-' ||
                formula[at] == '*' ||
                formula[at] == '/' ||
                formula[at] == 'd' ||
                formula[at] == 'D') {
                tokens.push_back(std::make_unique<tokenNode>(std::string(formula+at,1)));
                at++;
            } else {
                int from = at;
                for (;at < end && formula[at] >= '0' && formula[at] <= '9';at++);
                if (from != at) //zahl gelesen
                    tokens.push_back(std::make_unique<valueNode>(strtol(formula+from,NULL,10)));
                else { //keine zahl, kein operator
                    std::stringstream tmp;
                    tmp << "Unknown operator or character at index " << at << ": " << formula[at];
                    throw std::runtime_error(tmp.str().c_str());
                }
            }
        }
        std::unique_ptr<node> tree = parseDieTokenGroup(tokens.begin(), tokens.end());
        if (is_nodetype<tokenNode>(tree))
            throw std::runtime_error("Expression was not completely resolved!");

        return tree;
    }
}
